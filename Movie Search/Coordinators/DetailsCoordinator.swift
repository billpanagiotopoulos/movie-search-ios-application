//
//  MovieDetailsCoordinator.swift
//  MovieSearch - Default
//
//  Created by Vasilis Panagiotopoulos on 07/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import MovieDetails
import UIKit

class DetailsCoordinator: Coordinator {
    private let presenter: UINavigationController
    private var detailsViewModel: ShowDetailsViewModel
    private var cellViewModel: ShowDetailsCellViewModel
    
    init(presenter: UINavigationController,
         detailsViewModel: ShowDetailsViewModel,
         cellViewModel: ShowDetailsCellViewModel) {
        self.detailsViewModel = detailsViewModel
        self.cellViewModel = cellViewModel
        self.presenter = presenter
    }
    
    func start() {
        let detailsVC = ShowDetailsVC(nibName: ShowDetailsVC.className, bundle: Bundle(for: ShowDetailsVC.self))
        detailsVC.viewModel = detailsViewModel
        detailsVC.cellViewModel = cellViewModel
        presenter.pushViewController(detailsVC, animated: true)
    }
}
