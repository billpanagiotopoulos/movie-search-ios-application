//
//  Coordinator.swift
//  Movie Search
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

protocol Coordinator {
    func start()
}
