//
//  ApplicationCoordinator.swift
//  Movie Search
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit
import MovieSearch
import MovieDetails

class ApplicationCoordinator: Coordinator {
    let window: UIWindow
    let rootViewController: UINavigationController
    var movieDetailsCoordinator: DetailsCoordinator?
    
    init(window: UIWindow) {
        self.window = window
        
        let initialVC = SearchVC(nibName: SearchVC.className, bundle: Bundle(for: SearchVC.self))
        rootViewController = RootNavigationController(rootViewController: initialVC)

        // Assign view model
        initialVC.viewModel = searchViewModel
        
        // Get informed when a movie or show is selected
        initialVC.delegate = self
    }
    
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}

// MARK: Helpers
extension ApplicationCoordinator {
    var searchViewModel: MovieSearchViewModel {
        return MovieSearchViewModel(
            colorStyles: [
                .searchBarBackgroundColor: .searchBarBackgroundColor,
                .tableViewBackgroundColor: .tableViewBackgroundColor,
                .emptyDataSetFontColor: .emptyDataSetFontColor,
            ],
            fontStyles: [
                .emptyDataSetFont: .emptyDataSetFont
            ],
            searchCellColorStyles: [
                .titleLabelFontColor: .fontColor,
                .voteLabelFontColor: .fontColor,
                .releaseDateLabelFontColor: .fontColor,
                .separatorColor: .separatorColor,
                .posterBackgroundColor: .posterBackgroundColor
            ],
            searchCellFontStyles: [
                .titleLabelFont: .titleLabelFont,
                .voteLabelFont: .voteLabelFont,
                .releaseDateLabelFont: .releaseDateLabelFont
            ],
            searchCellIcons: [
                .rating: UIImage(named: "rating")!,
                .releaseDate: UIImage(named: "release-date")!,
                .defaultPosterImage: UIImage(named: "default-image")!
            ],
            movieDBAPIKey: Constants.MovieDBAPIKey,
            movieDBBasePosterURL: Constants.MovieDBPosterBaseURL
        )
    }
    
    var detailsViewModel: ShowDetailsViewModel {
        let viewModel = ShowDetailsViewModel()
        viewModel.titleFont = .detailsTitleFont
        viewModel.subTitleFont = .detailsSubTitleFont
        viewModel.summaryFont = .detailsSummaryFont
        viewModel.buttonFont = .detailsButtonFont
        
        // Colors
        viewModel.titleFontColor = .detailsTitleFontColor
        viewModel.subTitleFontColor = .detailsSubTitleFontColor
        viewModel.summaryFontColor = .detailsSummaryFontColor
        viewModel.buttonNormalFontColor = .detailsSummaryFontColor
        viewModel.buttonHighlightedFontColor = .detailsButtonHighlightedFontColor
        viewModel.buttonNormalBackgroundColor = .detailsButtonNormalBackgroundColor
        viewModel.buttonHighlightedBackgroundColor = .detailsButtonHighlightedBackgroundColor
        
        // Settings
        viewModel.placeholderImage = UIImage(named: "youtube")!
        viewModel.buttonImage = UIImage(named: "youtube")!
        viewModel.backImageBaseUrl = Constants.MovieDBBackImageBaseURL
        viewModel.youtubeBaseAppScheme = Constants.YoutubeBaseAppScheme
        viewModel.youtubeBaseUrl = Constants.YoutubeBaseURL

        return viewModel
    }
}

// MARK: - SearchVC Delegation
extension ApplicationCoordinator: SearchVCDelegate {
    func movieSelected(movieModel: TMDBMovieResponse, trailerModel: TMDBTrailerResponse) {
        movieDetailsCoordinator = DetailsCoordinator(presenter: rootViewController,
                                                     detailsViewModel: detailsViewModel,
                                                     cellViewModel: ShowDetailsCellViewModel.convert(from: movieModel, trailerResponse: trailerModel))
        movieDetailsCoordinator?.start()
    }
    
    func tvShowSelected(tvModel: TMDBTVShowResponse, trailerModel: TMDBTrailerResponse) {
        movieDetailsCoordinator = DetailsCoordinator(presenter: rootViewController,
                                                     detailsViewModel: detailsViewModel,
                                                     cellViewModel: ShowDetailsCellViewModel.convert(from: tvModel, trailerResponse: trailerModel))
        movieDetailsCoordinator?.start()

    }
}
