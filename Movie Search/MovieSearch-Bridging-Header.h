//
//  Movie Search-Bridging-Header.h
//  Movie Search
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#if DEFAULT_TARGET
    #import <DefaultTheme/DefaultTheme.h>
#elif BRAND1_TARGET
    #import <Brand1Theme/Brand1Theme.h>
#endif

#import <MovieDetails/MovieDetails.h>
