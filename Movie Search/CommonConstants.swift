//
//  CommonConstants.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

struct Constants {
    static let MovieDBAPIKey = "6b2e856adafcc7be98bdf0d8b076851c"
    static let MovieDBPosterBaseURL = "https://image.tmdb.org/t/p/w200"
    static let MovieDBBackImageBaseURL = "https://image.tmdb.org/t/p/w500"
    static let YoutubeBaseAppScheme = "youtube://%@"
    static let YoutubeBaseURL = "https://www.youtube.com/watch?v=%@"
}
