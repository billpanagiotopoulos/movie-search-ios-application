//
//  DefaultEnvironment.swift
//  MovieSearch - Default
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation
import TermiNetwork

enum Environment: TNEnvironmentProtocol {
    case production
    
    func configure() -> TNEnvironment {
        let requestConfiguration = TNRequestConfiguration(cachePolicy: .useProtocolCachePolicy,
                                                          timeoutInterval: 30,
                                                          requestBodyType: .JSON)
        switch self {
        case .production:
            return TNEnvironment(scheme: .https,
                                 host: "api.themoviedb.org",
                                 suffix: path("3"),
                                 requestConfiguration: requestConfiguration)
        }
    }
}
