//
//  DefaultTheme.h
//  DefaultTheme
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DefaultTheme.
FOUNDATION_EXPORT double DefaultThemeVersionNumber;

//! Project version string for DefaultTheme.
FOUNDATION_EXPORT const unsigned char DefaultThemeVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DefaultTheme/PublicHeader.h>
