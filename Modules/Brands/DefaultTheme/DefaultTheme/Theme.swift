//
//  Theme.swift
//  DefaultTheme
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

open class Theme {
    public static func apply() {
        UINavigationBar.appearance().barTintColor = .barTintColor
        UINavigationBar.appearance().tintColor = .tintColor
        
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.tintColor,
            NSAttributedString.Key.font: UIFont.fontWithStyle(.medium, size: 20)
        ]
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).tintColor = .searchBarTintColor
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).textColor = .searchBarTextColor
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = .searchBarBackgroundColor
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = .fontWithStyle(.medium, size: 16)
        
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([
            NSAttributedString.Key.foregroundColor: UIColor.searchBarTextColor
            ] , for: .normal)
        
    }
}
