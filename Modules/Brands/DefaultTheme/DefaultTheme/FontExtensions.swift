//
//  FontExtensions.swift
//  DefaultTheme
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

public extension UIFont {
    enum Style: String {
        case light = "AppleSDGothicNeo-Light"
        case regular = "AppleSDGothicNeo-Regular"
        case medium = "AppleSDGothicNeo-Medium"
        case bold = "AppleSDGothicNeo-SemiBold"
    }
    
    static func fontWithStyle(_ style: Style, size: CGFloat = 12.0) -> UIFont {
        return UIFont(name: style.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static let emptyDataSetFont = fontWithStyle(.light, size: 25)
    static let titleLabelFont = fontWithStyle(.medium, size: 16)
    static let voteLabelFont = fontWithStyle(.light, size: 12)
    static let releaseDateLabelFont = fontWithStyle(.light, size: 12)
    
    // Details
    static let detailsTitleFont = fontWithStyle(.bold, size: 20)
    static let detailsSubTitleFont = fontWithStyle(.medium, size: 15)
    static let detailsSummaryFont = fontWithStyle(.light, size: 15)
    static let detailsButtonFont = fontWithStyle(.light, size: 15)
}
