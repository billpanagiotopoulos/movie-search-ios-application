//
//  UIColorExtensions.swift
//  DefaultTheme
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

public extension UIColor {
    static let barTintColor = UIColor.orange
    static let tintColor = UIColor.white
    
    static let fontColor = UIColor.gray
    static let separatorColor = UIColor(red: 0.970, green: 0.870, blue: 0.898, alpha: 1.00)
    
    // Searchbar
    static let searchBarTintColor = UIColor.gray
    static let searchBarTextColor = UIColor.gray
    static let emptyDataSetFontColor = UIColor.gray
    static let searchBarBackgroundColor = UIColor.white
    static let tableViewBackgroundColor = UIColor.white
    
    // Search screen
    static let posterBackgroundColor = UIColor.clear
    
    // Colors
    static let detailsTitleFontColor = UIColor.fontColor
    static let detailsSubTitleFontColor = UIColor.fontColor
    static let detailsSummaryFontColor = UIColor.fontColor
    static let detailsButtonNormalFontColor = UIColor.fontColor
    static let detailsButtonHighlightedFontColor = UIColor.white
    static let detailsButtonNormalBackgroundColor = UIColor(red: 0.969, green: 0.867, blue: 0.898, alpha: 1.00)
    static let detailsButtonHighlightedBackgroundColor = UIColor.barTintColor

}
