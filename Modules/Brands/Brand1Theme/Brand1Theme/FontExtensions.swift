//
//  FontExtensions.swift
//  DefaultTheme
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

public extension UIFont {
    enum Style: String {
        case light = "HelveticaNeue-Light"
        case regular = "HelveticaNeue"
        case medium = "HelveticaNeue-Medium"
        case bold = "HelveticaNeue-Bold"
    }
    
    static func fontWithStyle(_ style: Style, size: CGFloat = 12.0) -> UIFont {
        return UIFont(name: style.rawValue, size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static let emptyDataSetFont = fontWithStyle(.light, size: 25)
    static let titleLabelFont = fontWithStyle(.medium, size: 15)
    static let voteLabelFont = fontWithStyle(.regular, size: 13)
    static let releaseDateLabelFont = fontWithStyle(.regular, size: 13)
    
    // Details
    static let detailsTitleFont = fontWithStyle(.bold, size: 20)
    static let detailsSubTitleFont = fontWithStyle(.medium, size: 15)
    static let detailsSummaryFont = fontWithStyle(.light, size: 15)
    static let detailsButtonFont = fontWithStyle(.light, size: 15)
}
