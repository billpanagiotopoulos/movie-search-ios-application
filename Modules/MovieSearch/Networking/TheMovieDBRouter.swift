//
//  TheMovieDBRouter.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation
import TermiNetwork

enum TheMovieDBRouter: TNRouterProtocol {
    case search(apiKey: String, term: String, page: Int)
    case showDetails(apiKey: String, type: String, id: String)
    case videoDetails(apiKey: String, type: String, id: String)
    
    func configure() -> TNRouteConfiguration {
        
        switch self {
        case .search(let apiKey, let term, let page):
            return TNRouteConfiguration(method: .get,
                                        path: path("search", "multi"),
                                        params: ["api_key": apiKey, "query": term, "page": String(page)])
        case .showDetails(let apiKey, let type, let id):
            return TNRouteConfiguration(method: .get,
                                        path: path(type, id),
                                        params: ["api_key": apiKey])
        case .videoDetails(let apiKey, let type, let id):
            return TNRouteConfiguration(method: .get,
                                        path: path(type, id, "videos"),
                                        params: ["api_key": apiKey])
        }
    }
}
