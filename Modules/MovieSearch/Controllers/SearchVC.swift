//
//  MovieSearchVC.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Common
import TermiNetwork
import SVProgressHUD

// Delegation to inform the outside world with the movie/tv show details
public protocol SearchVCDelegate: class {
    func movieSelected(movieModel: TMDBMovieResponse, trailerModel: TMDBTrailerResponse)
    func tvShowSelected(tvModel: TMDBTVShowResponse, trailerModel: TMDBTrailerResponse)
}

open class SearchVC: UIViewController {    
    // MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    open weak var delegate: SearchVCDelegate?
    
    // MARK: Private properties
    fileprivate let emptyDatasetTextPadding: CGFloat = 30.0
    fileprivate var cellViewModels: [MovieSearchCellViewModel] = [] {
        didSet {
            viewModel.tableViewFinishedLoading = false
            tableView.reloadData()
            DispatchQueue.main.async {
                self.viewModel.tableViewFinishedLoading = true
            }
        }
    }
    fileprivate var keyboardVisible: Bool = false
    fileprivate lazy var emptyDataSetStringAttributes: [NSAttributedString.Key : Any] = {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.firstLineHeadIndent = emptyDatasetTextPadding
        paragraphStyle.headIndent = emptyDatasetTextPadding
        paragraphStyle.tailIndent = -emptyDatasetTextPadding
        paragraphStyle.alignment = .center
        
        let attributes: [NSAttributedString.Key : Any] = [
            NSAttributedString.Key.font: viewModel?.fontStyles[.emptyDataSetFont] ?? UIFont.systemFont(ofSize: 15),
            NSAttributedString.Key.foregroundColor: viewModel?.colorStyles[.emptyDataSetFontColor] ?? .black,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
        ]
        return attributes
    }()
    
    // MARK: Public properties
    open var viewModel: MovieSearchViewModel!
    
    // MARK: View lifecycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Movie Search", comment: "MovieSearchVC")
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self

        tableView.register(UINib(nibName: SearchCell.className, bundle: Bundle(for: SearchCell.self)), forCellReuseIdentifier: SearchCell.className)
        
        setupUI()
    }
    
    override open var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
}

// MARK: UITableView delegation
extension SearchVC: UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellViewModels.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchCell.className, for: indexPath) as! SearchCell
        let viewModel = cellViewModels[indexPath.row]
        viewModel.colorStyles = self.viewModel?.searchCellColorStyles ?? [:]
        viewModel.fontStyles = self.viewModel?.searchCellFontStyles ?? [:]
        viewModel.icons = self.viewModel?.searchCellIcons ?? [:]
        cell.configureCell(withViewModel: cellViewModels[indexPath.row])
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        showDetails(withCellViewModel: cellViewModels[indexPath.row])
    }
}

// MARK: Helpers
extension SearchVC {
    fileprivate func setupUI() {
        searchBar.backgroundColor = viewModel?.colorStyles[.searchBarBackgroundColor]
        tableView.backgroundColor = viewModel?.colorStyles[.tableViewBackgroundColor]
    }
    
    fileprivate func changeSearchState(to searchState: MovieSearchViewModel.SearchState) {
        self.viewModel.searchState = searchState
        self.tableView.reloadEmptyDataSet()
    }
    
    fileprivate func performSearch(withTerm term: String, page: Int = 1, showProgress: Bool = false) {
        if showProgress {
            SVProgressHUD.show()
        }
        
        TNRouter.start(TheMovieDBRouter.search(apiKey: viewModel.movieDBAPIKey, term: term, page: page), responseType: TMDBSearchResponse.self, onSuccess: { response in
            self.viewModel?.totalPages = response.totalPages ?? 1
            self.viewModel?.currentPage = response.page ?? 1
            self.viewModel?.searchState = .none
            
            // Clear view models for first page
            if page == 1 {
                self.cellViewModels.removeAll()
            }
            
            self.cellViewModels.append(contentsOf: MovieSearchCellViewModel.convert(fromTMDBResponse: response, posterBaseURL: self.viewModel.movieDBBasePosterURL))
            
            self.changeSearchState(to: .none)
            SVProgressHUD.dismiss()
        }) { error, data in
            self.viewModel?.lastFailedRequestDate = Date()
            self.viewModel?.searchState = .error
            self.tableView.reloadData()
            if showProgress {
                SVProgressHUD.dismiss()
                SVProgressHUD.showError(withStatus: NSLocalizedString("An error occurred. Please try again.", comment: "MovieSearchVC"))
            }
        }
    }
    
    fileprivate func showDetails(withCellViewModel cellViewModel: MovieSearchCellViewModel) {
        SVProgressHUD.show()
        
        let route = TheMovieDBRouter.showDetails(apiKey: viewModel.movieDBAPIKey,
                                                 type: cellViewModel.type.rawValue,
                                                 id: String(cellViewModel.id))
        
        DispatchQueue.global(qos: .userInitiated).async {
            let downloadGroup = DispatchGroup()
            
            var videoRoute: TheMovieDBRouter?
            var finishedClosure: ((TMDBTrailerResponse)->())?
            downloadGroup.enter()

            switch cellViewModel.type {
            case .movie:
                TNRouter.start(route, responseType: TMDBMovieResponse.self, onSuccess: { response in
                    videoRoute = TheMovieDBRouter.videoDetails(apiKey: self.viewModel.movieDBAPIKey, type: cellViewModel.type.rawValue, id: String(response.id))
                    finishedClosure = { trailerResponse in
                        self.delegate?.movieSelected(movieModel: response, trailerModel: trailerResponse)
                    }
                    downloadGroup.leave()
                }) { error, data in
                    SVProgressHUD.showError(withStatus: NSLocalizedString("An error occurred. Please try again.", comment: "MovieSearchVC"))
                    downloadGroup.leave()
                }
            case .tvShow:
                TNRouter.start(route, responseType: TMDBTVShowResponse.self, onSuccess: { response in
                    videoRoute = TheMovieDBRouter.videoDetails(apiKey: self.viewModel.movieDBAPIKey, type: cellViewModel.type.rawValue, id: String(response.id))
                    finishedClosure = { trailerResponse in
                        self.delegate?.tvShowSelected(tvModel: response, trailerModel: trailerResponse)
                    }
                    downloadGroup.leave()
                }) { error, data in
                    SVProgressHUD.showError(withStatus: NSLocalizedString("An error occurred. Please try again.", comment: "MovieSearchVC"))
                    downloadGroup.leave()
                }
            }
            
            downloadGroup.wait()
            
            if let videoRoute = videoRoute {
                TNRouter.start(videoRoute, responseType: TMDBTrailerResponse.self, onSuccess: { response in
                    SVProgressHUD.dismiss()
                    finishedClosure?(response)
                }) { error, data in
                    self.viewModel?.lastFailedRequestDate = Date()
                    SVProgressHUD.showError(withStatus: NSLocalizedString("An error occurred. Please try again.", comment: "MovieSearchVC"))
                }
            }
        }
    }
}

// MARK: UISearchBar delegation
extension SearchVC: UISearchBarDelegate {
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        keyboardVisible = true
        searchBar.setShowsCancelButton(true, animated: true)
        changeSearchState(to: .typing)
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        keyboardVisible = false
        searchBar.setShowsCancelButton(false, animated: true)
        if searchBar.text.val.isEmpty {
            changeSearchState(to: .none)
        } else {
            changeSearchState(to: viewModel.searchState)
        }
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        tableView.setContentOffset(.zero, animated: false)

        // Check for empty search text
        guard !searchBar.text.val.isEmpty else {
            cellViewModels.removeAll()
            changeSearchState(to: .none)
            return
        }
        cellViewModels.removeAll()
        changeSearchState(to: .fetching)
        performSearch(withTerm: searchBar.text.val)
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        tableView.setContentOffset(.zero, animated: false)
        cellViewModels.removeAll()
        searchBar.setShowsCancelButton(false, animated: true)
        searchBar.text = nil
        searchBar.resignFirstResponder()
    }
}

// MARK:  DZNEmptyDataSet delegation
extension SearchVC: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        var message: String
        
        switch viewModel.searchState {
        case .fetching:
            message = NSLocalizedString("Searching...", comment: "MovieSearchVC")
        case .error:
            message = NSLocalizedString("An error occurred. Please try again.", comment: "MovieSearchVC")
        case .typing:
            message = keyboardVisible ? "" : NSLocalizedString("Please press on the search button to complete your search.", comment: "MovieSearchVC")
        case .none:
            if searchBar.text.val.isEmpty {
                message = NSLocalizedString("Please enter your search criteria.", comment: "MovieSearchVC")
            } else {
                message = NSLocalizedString("No results found based on your search criteria.", comment: "MovieSearchVC")
            }
        }
        
        return NSAttributedString(string: message, attributes: emptyDataSetStringAttributes)
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: viewModel?.searchState == .typing ? "start-typing" : "no-results")
    }
    
    public func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return -50
    }
}

// MARK: UIScrollView delegation
extension SearchVC: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView && tableView.isScrolledToBottom {
            if viewModel.canGoToNextPage {
                changeSearchState(to: .fetching)
                performSearch(withTerm: searchBar.text.val, page: viewModel.currentPage+1, showProgress: true)
            }
        }
    }
}
