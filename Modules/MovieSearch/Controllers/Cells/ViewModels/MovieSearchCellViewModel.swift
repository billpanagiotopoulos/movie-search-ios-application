//
//  MovieSearchCellViewModel.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation
import TermiNetwork
import Common

open class MovieSearchCellViewModel {
    // Model properties
    var posterUrl: String
    var showTitle: Observable<String>
    var voteAvarage: Float
    var releaseDate: Date
    var id: Int
    
    public enum EntryType: String {
        case movie = "movie"
        case tvShow = "tv"
    }
    
    // Styling
    public enum ColorStyleType {
        case titleLabelFontColor
        case voteLabelFontColor
        case releaseDateLabelFontColor
        case separatorColor
        case posterBackgroundColor
    }
    
    public enum FontStyleType {
        case titleLabelFont
        case voteLabelFont
        case releaseDateLabelFont
    }
    
    public enum IconType {
        case rating
        case releaseDate
        case defaultPosterImage
    }

    var type: EntryType = .movie
    var colorStyles: [ColorStyleType: UIColor] = [:]
    var fontStyles: [FontStyleType: UIFont] = [:]
    var icons: [IconType: UIImage] = [:]

    // Initializers
    init(id: Int, posterUrl: String, showTitle: String, voteAvarage: Float, releaseDate: Date) {
        self.id = id
        self.posterUrl = posterUrl
        self.showTitle = Observable(showTitle)
        self.voteAvarage = voteAvarage
        self.releaseDate = releaseDate
    }
    
    init(withTMDBResult model: TMDBSearchResult) {
        self.id = model.id.val
        self.type = EntryType(rawValue: model.mediaType.val) ?? .movie

        let title = type == .movie ? model.title.val : model.name.val
        let releaseDate = type == .movie ? model.releaseDate.val : model.firstAirDate.val
        
        self.showTitle = Observable(title)
        
        
        let itemType = type == .movie ? NSLocalizedString("Movie", comment: "MovieSearchCell") : NSLocalizedString("TV Show", comment: "MovieSearchCell")
        
        if model.title.val.isEmpty {
            showTitle.value = NSLocalizedString("n/a", comment: "MovieSearchCell")
        } else {
            showTitle.value = String(format: "%@ (%@)", title, itemType)
        }
        
        self.posterUrl = model.posterPath.val
        self.voteAvarage = model.voteAverage.val
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        self.releaseDate = dateFormatter.date(from: releaseDate) ?? Date(timeIntervalSince1970: 0)
    }
}

// MARK: Converters
extension MovieSearchCellViewModel {
    static func convert(fromTMDBResponse model: TMDBSearchResponse, posterBaseURL: String) -> [MovieSearchCellViewModel] {
        var items: [MovieSearchCellViewModel] = []
        model.results.val.forEach { model in
            if !model.posterPath.val.isEmpty {
                model.posterPath = posterBaseURL + model.posterPath.val
            }
            items.append(MovieSearchCellViewModel(withTMDBResult: model))
        }
        return items
    }
}

// MARK: Mock Data
extension MovieSearchCellViewModel {
    static func mockData(searchTerm: String = "") -> [MovieSearchCellViewModel] {
        let items = [
            MovieSearchCellViewModel(id: 1, posterUrl: "https://dummyimage.com/300/09f.png/fff", showTitle: "Spider-Man: Far From Home", voteAvarage: 8.3, releaseDate: Date.init(timeIntervalSince1970: 1375790863)),
            MovieSearchCellViewModel(id: 1, posterUrl: "https://dummyimage.com/300/09f.png/fff", showTitle: "Title 2", voteAvarage: 7.3, releaseDate: Date.init(timeIntervalSince1970: 1375710863)),
            MovieSearchCellViewModel(id: 1, posterUrl: "https://dummyimage.com/300/09f.png/fff", showTitle: "Title 3", voteAvarage: 5.4, releaseDate: Date.init(timeIntervalSince1970: 1375720863)),
            MovieSearchCellViewModel(id: 1, posterUrl: "https://dummyimage.com/300/09f.png/fff", showTitle: "Title 4", voteAvarage: 5.2, releaseDate: Date.init(timeIntervalSince1970: 1375740863)),
            MovieSearchCellViewModel(id: 1, posterUrl: "https://dummyimage.com/300/09f.png/fff", showTitle: "Title 5", voteAvarage: 8.1, releaseDate: Date.init(timeIntervalSince1970: 1375390863))
        ]
        
        return searchTerm.isEmpty ? items : items.filter { $0.showTitle.value.val.contains(searchTerm) }
    }
}
