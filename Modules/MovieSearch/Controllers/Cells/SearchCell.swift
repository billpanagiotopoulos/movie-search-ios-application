//
//  MovieSearchCell.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit
import Kingfisher

open class SearchCell: UITableViewCell {
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var releaseDateImageView: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
        
    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: Configuration
extension SearchCell {
    fileprivate func setupStyle(withViewModel viewModel: MovieSearchCellViewModel) {
        movieTitleLabel.textColor = viewModel.colorStyles[.titleLabelFontColor]
        ratingLabel.textColor = viewModel.colorStyles[.voteLabelFontColor]
        releaseDateLabel.textColor = viewModel.colorStyles[.releaseDateLabelFontColor]
        separatorView.backgroundColor = viewModel.colorStyles[.separatorColor]
        posterImageView.backgroundColor = viewModel.colorStyles[.posterBackgroundColor]

        movieTitleLabel.font = viewModel.fontStyles[.titleLabelFont]
        ratingLabel.font = viewModel.fontStyles[.voteLabelFont]
        releaseDateLabel.font = viewModel.fontStyles[.releaseDateLabelFont]
    }
    
    func configureCell(withViewModel viewModel: MovieSearchCellViewModel) {
        setupStyle(withViewModel: viewModel)
        
        posterImageView.kf.setImage(with: URL(string: viewModel.posterUrl), placeholder: viewModel.icons[.defaultPosterImage])
        ratingImageView.image = viewModel.icons[.rating]
        releaseDateImageView.image = viewModel.icons[.releaseDate]
        
        movieTitleLabel.bind(with: viewModel.showTitle)
        
        if viewModel.voteAvarage.isZero {
            ratingLabel.text = NSLocalizedString("n/a", comment: "MovieSearchCell")
        } else {
            ratingLabel.text = String(format: "%.2f", viewModel.voteAvarage)
        }
        
        if viewModel.releaseDate.timeIntervalSince1970 == 0 {
            releaseDateLabel.text = NSLocalizedString("n/a", comment: "MovieSearchCell")
        } else {
            let format = DateFormatter()
            format.dateFormat = "dd/MM/yyyy"
            releaseDateLabel.text = format.string(from: viewModel.releaseDate)
        }
    }
}
