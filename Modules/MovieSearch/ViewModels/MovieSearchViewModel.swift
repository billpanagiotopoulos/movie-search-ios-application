//
//  MovieSearchViewModel.swift
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

open class MovieSearchViewModel {
    // MARK: Styles
    public enum ColorStyleType {
        case searchBarBackgroundColor
        case tableViewBackgroundColor
        case emptyDataSetFontColor
        case separatorColor
    }
    
    public enum FontStyleType {
        case emptyDataSetFont
    }
    
    // MARK: Input State
    enum SearchState {
        case typing
        case fetching
        case error
        case none
    }
    
    open var colorStyles: [ColorStyleType: UIColor]
    open var fontStyles: [FontStyleType: UIFont]
    open var searchCellColorStyles: [MovieSearchCellViewModel.ColorStyleType: UIColor]
    open var searchCellFontStyles: [MovieSearchCellViewModel.FontStyleType: UIFont]
    open var searchCellIcons: [MovieSearchCellViewModel.IconType: UIImage]

    // Configuration
    open var movieDBAPIKey: String
    open var movieDBBasePosterURL: String
    
    var searchState: SearchState = .none
    var totalPages: Int = 0
    var currentPage: Int = 0
    var tableViewFinishedLoading = false
    var lastFailedRequestDate: Date = Date(timeIntervalSince1970: 0)
    var failedRequestRetryAfterSeconds: TimeInterval = 5
    
    var canGoToNextPage: Bool {
        return currentPage < totalPages && searchState != .fetching && tableViewFinishedLoading && (Date().timeIntervalSince1970 >= lastFailedRequestDate.timeIntervalSince1970 + failedRequestRetryAfterSeconds)
    }
    
    public init(colorStyles: [ColorStyleType: UIColor],
                fontStyles: [FontStyleType: UIFont],
                searchCellColorStyles: [MovieSearchCellViewModel.ColorStyleType: UIColor],
                searchCellFontStyles: [MovieSearchCellViewModel.FontStyleType: UIFont],
                searchCellIcons: [MovieSearchCellViewModel.IconType: UIImage],
                movieDBAPIKey: String,
                movieDBBasePosterURL: String) {
        self.colorStyles = colorStyles
        self.fontStyles = fontStyles
        self.searchCellColorStyles = searchCellColorStyles
        self.searchCellFontStyles = searchCellFontStyles
        self.searchCellIcons = searchCellIcons
        self.movieDBAPIKey = movieDBAPIKey
        self.movieDBBasePosterURL = movieDBBasePosterURL
    }
}
