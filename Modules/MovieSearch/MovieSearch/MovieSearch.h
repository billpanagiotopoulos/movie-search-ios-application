//
//  MovieSearch.h
//  MovieSearch
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MovieSearch.
FOUNDATION_EXPORT double MovieSearchVersionNumber;

//! Project version string for MovieSearch.
FOUNDATION_EXPORT const unsigned char MovieSearchVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MovieSearch/PublicHeader.h>
#import <Models/Models.h>
