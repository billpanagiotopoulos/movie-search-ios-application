//
//  DetailsViewModel.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsViewModel : NSObject
// Fonts
@property UIFont *titleFont;
@property UIFont *subTitleFont;
@property UIFont *summaryFont;
@property UIFont *buttonFont;

// Colors
@property UIColor *titleFontColor;
@property UIColor *subTitleFontColor;
@property UIColor *summaryFontColor;
@property UIColor *buttonNormalFontColor;
@property UIColor *buttonHighlightedFontColor;
@property UIColor *buttonNormalBackgroundColor;
@property UIColor *buttonHighlightedBackgroundColor;

// Settings
@property UIImage *placeholderImage;
@property UIImage *buttonImage;
@property NSString *backImageBaseUrl;
@property NSString *youtubeBaseAppScheme;
@property NSString *youtubeBaseUrl;
@end

NS_ASSUME_NONNULL_END
