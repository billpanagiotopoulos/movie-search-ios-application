//
//  ShowDetailsTrailerCell.m
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import "ShowDetailsTrailerCell.h"
#import "ShowDetailsTrailerViewModel.h"
#import "ShowDetailsViewModel.h"

@implementation ShowDetailsTrailerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// MARK: - Cell configuration

- (void)configureCellWithViewModel: (ShowDetailsTrailerViewModel *)cellViewModel
                  detailsViewModel: (ShowDetailsViewModel *)detailsViewModel {
    _viewModel = cellViewModel;
    [self setupUIWithDetailsViewModel: detailsViewModel];
    [_playButton setTitle:cellViewModel.title forState:UIControlStateNormal];
}

- (void)setupUIWithDetailsViewModel: (ShowDetailsViewModel *)detailsViewModel {
    _playButton.titleLabel.minimumScaleFactor = 0.5;
    _playButton.titleLabel.numberOfLines = 2;

    _playButton.titleLabel.font = detailsViewModel.buttonFont;
    [_playButton setTitleColor:detailsViewModel.buttonNormalFontColor forState:UIControlStateNormal];
    [_playButton setTitleColor:detailsViewModel.buttonHighlightedFontColor forState:UIControlStateHighlighted];
    [_playButton setBackgroundImage:[self imageWithColor:[detailsViewModel buttonNormalBackgroundColor]] forState:UIControlStateNormal];
    [_playButton setBackgroundImage:[self imageWithColor:[detailsViewModel buttonHighlightedBackgroundColor]] forState:UIControlStateHighlighted];
    [_playButton setImage:detailsViewModel.buttonImage forState:UIControlStateNormal];
    _playButton.clipsToBounds = true;
    _playButton.layer.cornerRadius = 6;
}

// MARK: - IBOutlets
- (IBAction)playButtonPressed:(id)sender {
    [_delegate playButtonPressedWithViewModel:_viewModel];
}

// MARK: - Helpers
- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


@end
