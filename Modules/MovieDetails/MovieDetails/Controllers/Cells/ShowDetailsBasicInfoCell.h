//
//  ShowDetailsBasicInfoCell.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewModels/ShowDetailsCellViewModel.h"

@class ShowDetailsViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsBasicInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *backdropImageView;
@property (weak, nonatomic) IBOutlet UILabel *showTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *showGenreLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *summaryValueLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backdropImageHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backdropImageAspectRatioConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backdropImageTopConstraint;

- (void)configureCellWithViewModel: (ShowDetailsCellViewModel *)cellViewModel
                  detailsViewModel: (ShowDetailsViewModel *)detailsViewModel;
@end

NS_ASSUME_NONNULL_END
