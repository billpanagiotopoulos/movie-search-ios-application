//
//  ShowDetailsTrailerViewModel.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 09/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TMDBTrailerResponse;

NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsTrailerViewModel : NSObject
@property NSString *title;
@property NSString *videoId;
@property NSString *type;
    
+ (NSMutableArray<ShowDetailsTrailerViewModel *> *)convertFromTrailerResponse:(TMDBTrailerResponse *)trailersResponse;
@end

NS_ASSUME_NONNULL_END
