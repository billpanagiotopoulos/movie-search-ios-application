//
//  ShowDetailsBasicInfoCellViewModel.m
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import "ShowDetailsCellViewModel.h"
#import <Models/Models-Swift.h>
#import "ShowDetailsTrailerViewModel.h"

@implementation ShowDetailsCellViewModel

- (instancetype)initWithShowTitle: (NSString *)showTitle
                    backdropImage: (NSString *)backdropImage
                      showSummary: (NSString *)showSummary
                            genre: (NSString *)genre
                             type: (NSString *)type
                         trailers: (NSMutableArray<ShowDetailsTrailerViewModel *>*)trailers {
    self = [super init];
    if (self) {
        self.showTitle = showTitle;
        self.backdropImage = backdropImage;
        self.showSummary = showSummary;
        self.genre = genre;
        self.type = type;
        self.trailers = trailers;
    }
    return self;
}

+ (ShowDetailsCellViewModel *)convertFromTVShowResponse: (TMDBTVShowResponse *)showResponse
                                        trailerResponse: (TMDBTrailerResponse *)trailerResponse {
    NSString *title = showResponse.name.length == 0 ? NSLocalizedString(@"n/a", "ShowDetailsCellViewModel"): showResponse.name;
    NSString *summary = showResponse.overview.length == 0 ? NSLocalizedString(@"n/a", "ShowDetailsCellViewModel"): showResponse.overview;
    NSString *type = NSLocalizedString(@"TV Show", "ShowDetailsCellViewModel");
    NSString *genre = showResponse.genres.count > 0 ? showResponse.genres[0].name : @"";
    
    return [[ShowDetailsCellViewModel alloc] initWithShowTitle:title
                                                 backdropImage:showResponse.backdropPath
                                                   showSummary:summary
                                                         genre:genre
                                                          type:type
                                                      trailers:[ShowDetailsTrailerViewModel convertFromTrailerResponse:trailerResponse]];
}

+ (ShowDetailsCellViewModel *)convertFromMovieResponse: (TMDBMovieResponse *)movieResponse
                                       trailerResponse: (TMDBTrailerResponse *)trailerResponse {
    
    NSString *title = movieResponse.title.length == 0 ? NSLocalizedString(@"n/a", "ShowDetailsCellViewModel"): movieResponse.title;
    NSString *summary = movieResponse.overview.length == 0 ? NSLocalizedString(@"n/a", "ShowDetailsCellViewModel"): movieResponse.overview;
    NSString *type = NSLocalizedString(@"Movie", "ShowDetailsCellViewModel");
    NSString *genre = movieResponse.genres.count > 0 ? movieResponse.genres[0].name : @"";
    
    return [[ShowDetailsCellViewModel alloc] initWithShowTitle:title
                                                 backdropImage:movieResponse.backdropPath
                                                   showSummary:summary
                                                         genre:genre
                                                          type:type
                                                      trailers:[ShowDetailsTrailerViewModel convertFromTrailerResponse:trailerResponse]];
}

+ (ShowDetailsCellViewModel *)mockData {
    return [[ShowDetailsCellViewModel alloc]
            initWithShowTitle:@"The Hobbit"
            backdropImage:@"https://dummyimage.com/300/09f.png/fff"
            showSummary: @"Bilbo Baggins is swept into a quest to reclaim the lost Dwarf Kingdom of Erebor from the fearsome dragon Smaug. Approached out of the blue by the wizard Gandalf the Grey, Bilbo finds himself joining a company of thirteen dwarves led by the legendary warrior, Thorin Oakenshield. Their journey will take them into the Wild; through treacherous lands swarming with Goblins and Orcs, deadly Wargs and Giant Spiders, Shapeshifters and Sorcerers. Although their goal lies to the East and the wastelands of the Lonely Mountain first they must escape the goblin tunnels, where Bilbo meets the creature that will change his life forever ... Gollum. Here, alone with Gollum, on the shores of an underground lake, the unassuming Bilbo Baggins not only discovers depths of guile and courage that surprise even him, he also gains possession of Gollum's precious ring that holds unexpected and useful qualities ... A simple, gold ring that is tied to the fate of all Middle-earth in ways Bilbo cannot begin to ... Written by Production"
            genre:@"Fantasy"
            type:@"Movie"
            trailers: [NSMutableArray array]];
}
@end
