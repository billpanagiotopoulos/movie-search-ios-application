//
//  ShowDetailsCellViewModel.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TMDBTVShowResponse, TMDBMovieResponse, ShowDetailsTrailerViewModel, TMDBTrailerResponse;

@interface ShowDetailsCellViewModel : NSObject
@property NSString *backdropImage;
@property NSString *showTitle;
@property NSString *showSummary;
@property NSString *genre;
@property NSString *type;
@property NSMutableArray<ShowDetailsTrailerViewModel *> *trailers;

+ (ShowDetailsCellViewModel *)mockData;
+ (ShowDetailsCellViewModel *)convertFromTVShowResponse: (TMDBTVShowResponse *)showResponse
                                        trailerResponse: (TMDBTrailerResponse *)trailerResponse;
+ (ShowDetailsCellViewModel *)convertFromMovieResponse: (TMDBMovieResponse *)movieResponse
                                       trailerResponse: (TMDBTrailerResponse *)trailerResponse;
@end

NS_ASSUME_NONNULL_END
