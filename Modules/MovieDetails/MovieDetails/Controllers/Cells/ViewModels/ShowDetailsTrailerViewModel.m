//
//  ShowDetailsTrailerViewModel.m
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 09/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import "ShowDetailsTrailerViewModel.h"
#import <Models/Models-Swift.h>

@implementation ShowDetailsTrailerViewModel

+ (NSMutableArray<ShowDetailsTrailerViewModel *> *)convertFromTrailerResponse:(TMDBTrailerResponse *)trailersResponse {
    NSMutableArray<ShowDetailsTrailerViewModel *> *items = [NSMutableArray array];
 
    for (TMDBTrailerResult *trailer in trailersResponse.results) {
        ShowDetailsTrailerViewModel *item = [[ShowDetailsTrailerViewModel alloc] init];
        item.title = trailer.name;
        item.videoId = trailer.key;
        item.type = trailer.site;
        [items addObject:item];
    }
    
    return items;
}

@end
