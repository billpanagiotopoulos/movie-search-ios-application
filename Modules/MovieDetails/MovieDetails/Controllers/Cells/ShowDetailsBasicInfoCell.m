//
//  ShowDetailsBasicInfoCell.m
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import "ShowDetailsBasicInfoCell.h"
#import <SDWebImage/SDWebImage.h>
#import "ShowDetailsTrailerViewModel.h"
#import "ShowDetailsViewModel.h"

@implementation ShowDetailsBasicInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

// MARK: - Cell configuration

- (void)configureCellWithViewModel: (ShowDetailsCellViewModel *)cellViewModel
                  detailsViewModel: (ShowDetailsViewModel *)detailsViewModel;
{
    [self setupUIWithDetailsViewModel:detailsViewModel];
    
    bool shouldHideBackdropImage = cellViewModel.backdropImage.length == 0;
    
    _backdropImageTopConstraint.constant = shouldHideBackdropImage ? 0 : 5;
    _backdropImageHeightConstraint.constant = shouldHideBackdropImage ? 0 : 300;
    _backdropImageHeightConstraint.priority = shouldHideBackdropImage ? 1000 : 900;
    _backdropImageAspectRatioConstraint.priority = shouldHideBackdropImage ? 900 : 1000;

    if (!shouldHideBackdropImage) {
        [_backdropImageView sd_setImageWithURL:[NSURL URLWithString:[detailsViewModel.backImageBaseUrl stringByAppendingString:cellViewModel.backdropImage]]
                              placeholderImage: nil];
    }
    
    _showTitleLabel.text = cellViewModel.showTitle;
    _summaryValueLabel.text = cellViewModel.showSummary;
    
    NSMutableArray<NSString *> *subTitleItems = [NSMutableArray array];
    [subTitleItems addObject:cellViewModel.type];
    
    if (cellViewModel.genre.length > 0) {
        [subTitleItems addObject:cellViewModel.genre];
    }
    
    _showGenreLabel.text = [subTitleItems componentsJoinedByString:@" • "];
}

- (void)setupUIWithDetailsViewModel: (ShowDetailsViewModel *)detailsViewModel {
    _showTitleLabel.font = detailsViewModel.titleFont;
    _showGenreLabel.font = detailsViewModel.subTitleFont;
    _summaryTitleLabel.font = detailsViewModel.subTitleFont;
    _summaryValueLabel.font = detailsViewModel.summaryFont;
    
    _showTitleLabel.textColor = detailsViewModel.titleFontColor;
    _showGenreLabel.textColor = detailsViewModel.subTitleFontColor;
    _summaryTitleLabel.textColor = detailsViewModel.subTitleFontColor;
    _summaryValueLabel.textColor = detailsViewModel.summaryFontColor;
}

@end
