//
//  ShowDetailsTrailerCell.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 08/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShowDetailsTrailerViewModel, ShowDetailsTrailerCellDelegate, ShowDetailsViewModel;

NS_ASSUME_NONNULL_BEGIN

@protocol ShowDetailsTrailerCellDelegate <NSObject>
- (void)playButtonPressedWithViewModel: (ShowDetailsTrailerViewModel *)viewModel;
@end

@interface ShowDetailsTrailerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) ShowDetailsTrailerViewModel* viewModel;
@property(nonatomic, weak) id<ShowDetailsTrailerCellDelegate> delegate;

- (void)configureCellWithViewModel: (ShowDetailsTrailerViewModel *)cellViewModel
                  detailsViewModel: (ShowDetailsViewModel *)detailsViewModel;
@end

NS_ASSUME_NONNULL_END
