//
//  MovieDetailsVC.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 07/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShowDetailsCellViewModel, ShowDetailsViewModel;

NS_ASSUME_NONNULL_BEGIN

@interface ShowDetailsVC : UIViewController
@property ShowDetailsViewModel *viewModel;
@property ShowDetailsCellViewModel *cellViewModel;
@end

NS_ASSUME_NONNULL_END
