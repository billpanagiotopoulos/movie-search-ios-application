//
//  MovieDetailsVC.m
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 07/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import "ShowDetailsVC.h"
#import "ShowDetailsBasicInfoCell.h"
#import "ShowDetailsTrailerCell.h"
#import "ShowDetailsTrailerViewModel.h"
#import "ShowDetailsViewModel.h"

@interface ShowDetailsVC ()<UITableViewDelegate, UITableViewDataSource, ShowDetailsTrailerCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *cells;
@end

@implementation ShowDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
        
    self.title = _cellViewModel.showTitle;
    
    [_tableView registerNib:[UINib nibWithNibName: NSStringFromClass([ShowDetailsBasicInfoCell class])
                                           bundle:[NSBundle bundleForClass:[ShowDetailsBasicInfoCell class]]] forCellReuseIdentifier:NSStringFromClass([ShowDetailsBasicInfoCell class])];
    [_tableView registerNib:[UINib nibWithNibName: NSStringFromClass([ShowDetailsTrailerCell class])
                                           bundle:[NSBundle bundleForClass:[ShowDetailsTrailerCell class]]] forCellReuseIdentifier:NSStringFromClass([ShowDetailsTrailerCell class])];

    [self initializeTableViewCells];
}

- (void)initializeTableViewCells {
    _cells = [NSMutableArray array];
    __weak ShowDetailsVC *weakSelf = self;

    [_cells addObject:^(NSIndexPath *indexPath) {
        ShowDetailsBasicInfoCell *cell = [weakSelf.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShowDetailsBasicInfoCell class])
                                                                         forIndexPath:indexPath];
        [cell configureCellWithViewModel:weakSelf.cellViewModel
                        detailsViewModel:weakSelf.viewModel];
        return cell;
    }];
    
    for (ShowDetailsTrailerViewModel *trailer in _cellViewModel.trailers) {
        // Support only youtube videos at the moment
        if (![trailer.type.lowercaseString isEqualToString:@"youtube"]) {
            continue;
        }
        
        [_cells addObject:^(NSIndexPath *indexPath) {
            ShowDetailsTrailerCell *cell = [weakSelf.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShowDetailsTrailerCell class])
                                                                                    forIndexPath:indexPath];
            [cell configureCellWithViewModel:trailer
                            detailsViewModel:weakSelf.viewModel];
            cell.delegate = weakSelf;
            return cell;
        }];
    }
}

// MARK: - UITableView delegation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cells.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * (^ cellBlock)(NSIndexPath *) = _cells[indexPath.row];
    return cellBlock(indexPath);
}

// MARK: - ShowDetailsTrailerCell delegation
- (void)playButtonPressedWithViewModel:(ShowDetailsTrailerViewModel *)viewModel {
    NSURL *appURL = [NSURL URLWithString:[NSString stringWithFormat:_viewModel.youtubeBaseAppScheme, viewModel.videoId]];
    NSURL *browserURL = [NSURL URLWithString:[NSString stringWithFormat:_viewModel.youtubeBaseUrl, viewModel.videoId]];

    if ([UIApplication.sharedApplication canOpenURL:appURL]) {
        [UIApplication.sharedApplication openURL:appURL options:@{} completionHandler:nil];
    } else {
        [UIApplication.sharedApplication openURL:browserURL options:@{} completionHandler:nil];
    }
}

@end
