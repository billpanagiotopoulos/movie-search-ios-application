//
//  MovieDetails.h
//  MovieDetails
//
//  Created by Vasilis Panagiotopoulos on 05/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MovieDetails.
FOUNDATION_EXPORT double MovieDetailsVersionNumber;

//! Project version string for MovieDetails.
FOUNDATION_EXPORT const unsigned char MovieDetailsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MovieDetails/PublicHeader.h>

#import "ShowDetailsVC.h"
#import "ShowDetailsCellViewModel.h"
#import "ShowDetailsViewModel.h"

#import <Models/Models-Swift.h>
