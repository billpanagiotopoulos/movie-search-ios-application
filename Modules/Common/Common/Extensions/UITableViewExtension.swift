//
//  UITableViewExtension.swift
//  Common
//
//  Created by Vasilis Panagiotopoulos on 10/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

public extension UITableView {
    var isScrolledToBottom: Bool {
        let bounds = self.bounds

        let offset = contentOffset
        let size = contentSize
        let inset = contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        let reload_distance:CGFloat = 10.0
        return y > h + reload_distance
    }
}
