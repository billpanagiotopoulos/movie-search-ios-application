//
//  Bindable.swift
//
//  Created by Vasilis Panagiotopoulos on 04/10/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

fileprivate struct AssociatedKeys {
    static var binder: UInt8 = 0
    static var valueChanged: UInt8 = 0
}

public protocol Bindable: NSObjectProtocol {
    associatedtype BindingType: Equatable
    func observingValue() -> BindingType?
    func updateValue(with value: BindingType)
    func bind(with observable: Observable<BindingType>)
}

extension Bindable where Self: NSObject {
    private var binder: Observable<BindingType> {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.binder) as? Observable<BindingType> else {
                let newValue = Observable<BindingType>()
                return newValue
            }
            
            return value
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.binder, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func register(for observable: Observable<BindingType>) {
        binder = observable
    }
    
    public func bind(with observable: Observable<BindingType>) {
        if let _self = self as? UIControl {
            _self.onValueChanged { [weak self] in
                if let binderValue = self?.binder.value, let obsevingValue = self?.observingValue(), binderValue != obsevingValue {
                    self?.binder.value = obsevingValue
                    self?.updateValue(with: obsevingValue)
                }
            }
        }
        
        binder = observable
        observable.bind { (_, value) in
            self.updateValue(with: value)
        }
    }
}
