//
//  UIControlBindableExtensions.swift
//
//  Created by Vasilis Panagiotopoulos on 04/10/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

extension UITextField: Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return text
    }
    
    public func updateValue(with value: String) {
        text = value
    }
}

extension UISlider: Bindable {
    public typealias BindingType = Float
    
    public func observingValue() -> Float? {
        return value
    }
    
    public func updateValue(with value: Float) {
        self.value = value
    }
}

extension UIStepper: Bindable {
    public typealias BindingType = Double
    
    public func observingValue() -> Double? {
        return value
    }
    
    public func updateValue(with value: Double) {
        self.value = value
    }
}

extension UISwitch: Bindable {
    public typealias BindingType = Bool
    
    public func observingValue() -> Bool? {
        return isOn
    }
    
    public func updateValue(with value: Bool) {
        isOn = value
    }
}

extension UILabel: Bindable {
    public typealias BindingType = String
    
    public func observingValue() -> String? {
        return text
    }
    
    public func updateValue(with value: String) {
        text = value
    }
}
