//
//  UIControlExtensions.swift
//
//  Created by Vasilis Panagiotopoulos on 04/10/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

fileprivate struct AssociatedKeys {
    static var valueChanged: UInt8 = 0
}

extension UIControl {
    typealias closure = ()->()
    
    private var onValueChanged: closure {
        get {
            guard let value = objc_getAssociatedObject(self, &AssociatedKeys.valueChanged) as? closure else {
                return { }
            }
            
            return value
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.valueChanged, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func onValueChanged(_ execute: @escaping closure) {
        self.addTarget(self, action: #selector(self.valueChanged), for: [.valueChanged, .editingChanged])
        onValueChanged = execute
    }
    
    @objc func valueChanged() {
        onValueChanged()
    }
}
