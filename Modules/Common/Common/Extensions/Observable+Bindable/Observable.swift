//
//  Observable.swift
//
//  Created by Vasilis Panagiotopoulos on 04/10/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

open class Observable<ObservedType> {
    public typealias Observer = (_ observable: Observable<ObservedType>, ObservedType) -> Void
    
    private var observers: [Observer]
    
    public var value: ObservedType? {
        didSet {
            if let value = value {
                notifyObservers(value)
            }
        }
    }
    
    public init(_ value: ObservedType? = nil) {
        self.value = value
        observers = []
    }
    
    func bind(observer: @escaping Observer) {
        observers.append(observer)
        
        if let value = value {
            observer(self, value)
        }
    }
    
    private func notifyObservers(_ value: ObservedType) {
        observers.forEach({ [unowned self] observer in
            observer(self, value)
        })
    }
}
