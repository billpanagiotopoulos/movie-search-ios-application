//
//  NSObjectExtensions.swift
//  Common
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

public protocol ClassNameProtocol {
    static var className: String { get }
}

extension ClassNameProtocol {
    public static var className: String {
        return String.init(describing: self)
    }
}

extension NSObject: ClassNameProtocol {}
