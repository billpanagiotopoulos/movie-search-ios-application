//
//  DefaultValuesExtensions.swift
//  Common
//
//  Created by Vasilis Panagiotopoulos on 06/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import Foundation

open class BaseClass {
    required public init() {}
}

public extension Optional where Wrapped: Collection, Wrapped.Iterator.Element: Any {
    var val: Wrapped {
        return self ?? [Wrapped.Iterator.Element]() as! Wrapped
    }
}
public extension Optional where Wrapped: BaseClass {
    var val: Wrapped {
        return self ?? Wrapped()
    }
}
public extension Optional where Wrapped == String {
    var val: String {
        return self ?? ""
    }
}
public extension Optional where Wrapped == Int {
    var val: Wrapped {
        return self ?? 0
    }
}
public extension Optional where Wrapped == Float {
    var val: Wrapped {
        return self ?? 0
    }
}
public extension Optional where Wrapped == Double {
    var val: Wrapped {
        return self ?? 0
    }
}
public extension Optional where Wrapped == Bool {
    var val: Wrapped {
        return self ?? false
    }
}
