//
//  Utilities.swift
//  Common
//
//  Created by Vasilis Panagiotopoulos on 07/08/2019.
//  Copyright © 2019 Vasilis Panagiotopoulos. All rights reserved.
//

import UIKit

public func delay(_ delay: Double, closure: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}
