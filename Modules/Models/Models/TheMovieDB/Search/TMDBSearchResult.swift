//
//	TMDBSearchResult.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

open class TMDBSearchResult :Codable {
	public let adult : Bool?
	public let backdropPath : String?
	public let firstAirDate : String?
	public let genreIds : [Int]?
	public let id : Int?
	public let mediaType : String?
	public let name : String?
	public let originCountry : [String]?
	public let originalLanguage : String?
	public let originalName : String?
	public let originalTitle : String?
	public let overview : String?
	public let popularity : Float?
	public var posterPath : String?
	public let releaseDate : String?
	public let title : String?
	public let video : Bool?
	public let voteAverage : Float?
	public let voteCount : Int?

	enum CodingKeys: String, CodingKey {
		case adult = "adult"
		case backdropPath = "backdrop_path"
		case firstAirDate = "first_air_date"
		case genreIds = "genre_ids"
		case id = "id"
		case mediaType = "media_type"
		case name = "name"
		case originCountry = "origin_country"
		case originalLanguage = "original_language"
		case originalName = "original_name"
		case originalTitle = "original_title"
		case overview = "overview"
		case popularity = "popularity"
		case posterPath = "poster_path"
		case releaseDate = "release_date"
		case title = "title"
		case video = "video"
		case voteAverage = "vote_average"
		case voteCount = "vote_count"
	}
    
    required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
		adult = try values.decodeIfPresent(Bool.self, forKey: .adult)
		backdropPath = try values.decodeIfPresent(String.self, forKey: .backdropPath)
		firstAirDate = try values.decodeIfPresent(String.self, forKey: .firstAirDate)
		genreIds = try values.decodeIfPresent([Int].self, forKey: .genreIds)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		mediaType = try values.decodeIfPresent(String.self, forKey: .mediaType)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		originCountry = try values.decodeIfPresent([String].self, forKey: .originCountry)
		originalLanguage = try values.decodeIfPresent(String.self, forKey: .originalLanguage)
		originalName = try values.decodeIfPresent(String.self, forKey: .originalName)
		originalTitle = try values.decodeIfPresent(String.self, forKey: .originalTitle)
		overview = try values.decodeIfPresent(String.self, forKey: .overview)
		popularity = try values.decodeIfPresent(Float.self, forKey: .popularity)
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
		releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		video = try values.decodeIfPresent(Bool.self, forKey: .video)
		voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage)
		voteCount = try values.decodeIfPresent(Int.self, forKey: .voteCount)
	}


}
