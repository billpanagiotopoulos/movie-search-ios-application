//
//	TMDBMovieSpokenLanguage.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

open class TMDBMovieSpokenLanguage: NSObject, Codable {
	@objc public let iso6391 : String?
	@objc public let name : String?

	enum CodingKeys: String, CodingKey {
		case iso6391 = "iso_639_1"
		case name = "name"
	}
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		iso6391 = try values.decodeIfPresent(String.self, forKey: .iso6391)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}
}
