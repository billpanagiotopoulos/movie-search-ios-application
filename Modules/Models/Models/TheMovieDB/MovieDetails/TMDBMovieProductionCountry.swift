//
//	TMDBMovieProductionCountry.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc open class TMDBMovieProductionCountry: NSObject, Codable {

	@objc public let iso31661 : String?
	@objc public let name : String?

	enum CodingKeys: String, CodingKey {
		case iso31661 = "iso_3166_1"
		case name = "name"
	}
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		iso31661 = try values.decodeIfPresent(String.self, forKey: .iso31661)
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}
}
