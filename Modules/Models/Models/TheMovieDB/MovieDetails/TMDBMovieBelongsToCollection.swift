//
//	TMDBMovieBelongsToCollection.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc open class TMDBMovieBelongsToCollection: NSObject, Codable {

	@objc public let backdropPath : String?
	@objc public var id : Int = -1
	@objc public let name : String?
	@objc public let posterPath : String?

	enum CodingKeys: String, CodingKey {
		case backdropPath = "backdrop_path"
		case id = "id"
		case name = "name"
		case posterPath = "poster_path"
	}
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		backdropPath = try values.decodeIfPresent(String.self, forKey: .backdropPath)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		name = try values.decodeIfPresent(String.self, forKey: .name)
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
	}


}
