//
//	TMDBMovieResponse.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

@objc open class TMDBMovieResponse: NSObject, Codable {
	@objc public var adult : Bool = false
	@objc public let backdropPath : String?
	@objc public let belongsToCollection : TMDBMovieBelongsToCollection?
	@objc public var budget : Int = -1
	@objc public let genres : [TMDBMovieGenre]?
	@objc public let homepage : String?
	@objc public var id : Int = -1
	@objc public let imdbId : String?
	@objc public let originalLanguage : String?
	@objc public let originalTitle : String?
	@objc public let overview : String?
	@objc public var popularity : Float = 0.0
	@objc public let posterPath : String?
	@objc public let productionCompanies : [TMDBMovieProductionCompany]?
	@objc public let productionCountries : [TMDBMovieProductionCountry]?
	@objc public let releaseDate : String?
	@objc public var revenue : Int = -1
	@objc public var runtime : Int = -1
	@objc public var spokenLanguages : [TMDBMovieSpokenLanguage] = []
	@objc public let status : String?
	@objc public let tagline : String?
	@objc public let title : String?
	@objc public var video : Bool = false
	@objc public var voteAverage : Float = 0.0
	@objc public var voteCount : Int = -1

	enum CodingKeys: String, CodingKey {
		case adult = "adult"
		case backdropPath = "backdrop_path"
		case belongsToCollection
		case budget = "budget"
		case genres = "genres"
		case homepage = "homepage"
		case id = "id"
		case imdbId = "imdb_id"
		case originalLanguage = "original_language"
		case originalTitle = "original_title"
		case overview = "overview"
		case popularity = "popularity"
		case posterPath = "poster_path"
		case productionCompanies = "production_companies"
		case productionCountries = "production_countries"
		case releaseDate = "release_date"
		case revenue = "revenue"
		case runtime = "runtime"
		case spokenLanguages = "spoken_languages"
		case status = "status"
		case tagline = "tagline"
		case title = "title"
		case video = "video"
		case voteAverage = "vote_average"
		case voteCount = "vote_count"
	}
    
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		adult = try values.decodeIfPresent(Bool.self, forKey: .adult) ?? false
		backdropPath = try values.decodeIfPresent(String.self, forKey: .backdropPath)
		belongsToCollection = try TMDBMovieBelongsToCollection(from: decoder)
		budget = try values.decodeIfPresent(Int.self, forKey: .budget) ?? 0
		genres = try values.decodeIfPresent([TMDBMovieGenre].self, forKey: .genres)
		homepage = try values.decodeIfPresent(String.self, forKey: .homepage)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
		imdbId = try values.decodeIfPresent(String.self, forKey: .imdbId)
		originalLanguage = try values.decodeIfPresent(String.self, forKey: .originalLanguage)
		originalTitle = try values.decodeIfPresent(String.self, forKey: .originalTitle)
		overview = try values.decodeIfPresent(String.self, forKey: .overview)
		popularity = try values.decodeIfPresent(Float.self, forKey: .popularity) ?? 0.0
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
		productionCompanies = try values.decodeIfPresent([TMDBMovieProductionCompany].self, forKey: .productionCompanies)
		productionCountries = try values.decodeIfPresent([TMDBMovieProductionCountry].self, forKey: .productionCountries)
		releaseDate = try values.decodeIfPresent(String.self, forKey: .releaseDate)
		revenue = try values.decodeIfPresent(Int.self, forKey: .revenue) ?? 0
		runtime = try values.decodeIfPresent(Int.self, forKey: .runtime) ?? 0
		spokenLanguages = try values.decodeIfPresent([TMDBMovieSpokenLanguage].self, forKey: .spokenLanguages) ?? []
		status = try values.decodeIfPresent(String.self, forKey: .status)
		tagline = try values.decodeIfPresent(String.self, forKey: .tagline)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		video = try values.decodeIfPresent(Bool.self, forKey: .video) ?? false
		voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage) ?? 0.0
		voteCount = try values.decodeIfPresent(Int.self, forKey: .voteCount) ?? -1
	}
}
