//
//	TVShowNetwork.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

@objc open class TMDBTVShowNetwork: NSObject, Codable {
	@objc public var id : Int = -1
	@objc public let logoPath : String?
	@objc public let name : String?
	@objc public let originCountry : String?

	enum CodingKeys: String, CodingKey {
		case id = "id"
		case logoPath = "logo_path"
		case name = "name"
		case originCountry = "origin_country"
	}
	required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		logoPath = try values.decodeIfPresent(String.self, forKey: .logoPath)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		originCountry = try values.decodeIfPresent(String.self, forKey: .originCountry)
	}
}
