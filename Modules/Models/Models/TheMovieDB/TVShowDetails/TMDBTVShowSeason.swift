//
//	TVShowSeason.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

 open class TMDBTVShowSeason: NSObject, Codable {
	@objc public let airDate : String?
    @objc public var episodeCount : Int = -1
    @objc public var id : Int = -1
	@objc public let name : String?
	@objc public let overview : String?
	@objc public let posterPath : String?
    @objc public var seasonNumber : Int = -1

	enum CodingKeys: String, CodingKey {
		case airDate = "air_date"
		case episodeCount = "episode_count"
		case id = "id"
		case name = "name"
		case overview = "overview"
		case posterPath = "poster_path"
		case seasonNumber = "season_number"
	}
    
	required public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
		airDate = try values.decodeIfPresent(String.self, forKey: .airDate)
        episodeCount = try values.decodeIfPresent(Int.self, forKey: .episodeCount) ?? -1
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		name = try values.decodeIfPresent(String.self, forKey: .name)
		overview = try values.decodeIfPresent(String.self, forKey: .overview)
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
		seasonNumber = try values.decodeIfPresent(Int.self, forKey: .seasonNumber) ?? -1
	}
}
