//
//	TVShowGenre.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc open class TMDBTVShowGenre : NSObject, Codable {
    @objc public var id : Int = -1
	@objc public let name : String?

    enum CodingKeys: String, CodingKey {
		case id = "id"
		case name = "name"
	}
	
    required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		name = try values.decodeIfPresent(String.self, forKey: .name)
	}


}
