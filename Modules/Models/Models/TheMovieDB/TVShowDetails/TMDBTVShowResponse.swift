//
//	TVShowResponse.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc open class TMDBTVShowResponse: NSObject, Codable {
	@objc public let backdropPath : String?
	@objc public let createdBy : [TMDBTVShowCreatedBy]?
	@objc public let episodeRunTime : [Int]?
	@objc public let firstAirDate : String?
	@objc public let genres : [TMDBTVShowGenre]?
	@objc public let homepage : String?
    @objc public var id : NSInteger = 0
    @objc public var inProduction : Bool = false
	@objc public let languages : [String]?
	@objc public let lastAirDate : String?
	@objc public let lastEpisodeToAir : TMDBTVShowLastEpisodeToAir?
	@objc public let name : String?
	@objc public let networks : [TMDBTVShowNetwork]?
    @objc public var numberOfEpisodes : Int = -1
    @objc public var numberOfSeasons : Int = -1
	@objc public let originCountry : [String]?
	@objc public let originalLanguage : String?
	@objc public let originalName : String?
	@objc public let overview : String?
    @objc public var popularity : Float = 0.0
	@objc public let posterPath : String?
	@objc public let productionCompanies : [TMDBTVShowProductionCompany]?
	@objc public let seasons : [TMDBTVShowSeason]?
	@objc public let status : String?
	@objc public let type : String?
    @objc public var voteAverage : Float = 0.0
    @objc public var voteCount : Int = 0

	enum CodingKeys: String, CodingKey {
		case backdropPath = "backdrop_path"
		case createdBy = "created_by"
		case episodeRunTime = "episode_run_time"
		case firstAirDate = "first_air_date"
		case genres = "genres"
		case homepage = "homepage"
		case id = "id"
		case inProduction = "in_production"
		case languages = "languages"
		case lastAirDate = "last_air_date"
		case lastEpisodeToAir
		case name = "name"
		case networks = "networks"
		case numberOfEpisodes = "number_of_episodes"
		case numberOfSeasons = "number_of_seasons"
		case originCountry = "origin_country"
		case originalLanguage = "original_language"
		case originalName = "original_name"
		case overview = "overview"
		case popularity = "popularity"
		case posterPath = "poster_path"
		case productionCompanies = "production_companies"
		case seasons = "seasons"
		case status = "status"
		case type = "type"
		case voteAverage = "vote_average"
		case voteCount = "vote_count"
	}
    
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		backdropPath = try values.decodeIfPresent(String.self, forKey: .backdropPath)
		createdBy = try values.decodeIfPresent([TMDBTVShowCreatedBy].self, forKey: .createdBy)
		episodeRunTime = try values.decodeIfPresent([Int].self, forKey: .episodeRunTime)
		firstAirDate = try values.decodeIfPresent(String.self, forKey: .firstAirDate)
		genres = try values.decodeIfPresent([TMDBTVShowGenre].self, forKey: .genres)
		homepage = try values.decodeIfPresent(String.self, forKey: .homepage)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		inProduction = try values.decodeIfPresent(Bool.self, forKey: .inProduction) ?? false
		languages = try values.decodeIfPresent([String].self, forKey: .languages)
		lastAirDate = try values.decodeIfPresent(String.self, forKey: .lastAirDate)
		lastEpisodeToAir = try TMDBTVShowLastEpisodeToAir(from: decoder)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		networks = try values.decodeIfPresent([TMDBTVShowNetwork].self, forKey: .networks)
		numberOfEpisodes = try values.decodeIfPresent(Int.self, forKey: .numberOfEpisodes) ?? -1
		numberOfSeasons = try values.decodeIfPresent(Int.self, forKey: .numberOfSeasons) ?? -1
		originCountry = try values.decodeIfPresent([String].self, forKey: .originCountry)
		originalLanguage = try values.decodeIfPresent(String.self, forKey: .originalLanguage)
		originalName = try values.decodeIfPresent(String.self, forKey: .originalName)
		overview = try values.decodeIfPresent(String.self, forKey: .overview)
		popularity = try values.decodeIfPresent(Float.self, forKey: .popularity) ?? 0.0
		posterPath = try values.decodeIfPresent(String.self, forKey: .posterPath)
		productionCompanies = try values.decodeIfPresent([TMDBTVShowProductionCompany].self, forKey: .productionCompanies)
		seasons = try values.decodeIfPresent([TMDBTVShowSeason].self, forKey: .seasons)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage) ?? 0.0
		voteCount = try values.decodeIfPresent(Int.self, forKey: .voteCount) ?? -1
	}
}
