//
//	TVShowCreatedBy.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc open class TMDBTVShowCreatedBy: NSObject, Codable {
	@objc public let creditId : String?
    @objc public var gender : Int = -1
    @objc public var id : Int = -1
	@objc public let name : String?
	@objc public let profilePath : String?

	enum CodingKeys: String, CodingKey {
		case creditId = "credit_id"
		case gender = "gender"
		case id = "id"
		case name = "name"
		case profilePath = "profile_path"
	}
    
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		creditId = try values.decodeIfPresent(String.self, forKey: .creditId)
        gender = try values.decodeIfPresent(Int.self, forKey: .gender) ?? -1
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		name = try values.decodeIfPresent(String.self, forKey: .name)
		profilePath = try values.decodeIfPresent(String.self, forKey: .profilePath)
	}
}
