//
//	TVShowLastEpisodeToAir.swift
//
//	Create by Vasilis Panagiotopoulos on 7/8/2019
//	Copyright © 2019. All rights reserved.

import Foundation

@objc public class TMDBTVShowLastEpisodeToAir: NSObject, Codable {

	@objc public let airDate : String?
    @objc public var episodeNumber : Int = -1
    @objc public var id : Int = -1
	@objc public let name : String?
	@objc public let overview : String?
	@objc public let productionCode : String?
    @objc public var seasonNumber : Int = -1
    @objc public var showId : Int = -1
	@objc public let stillPath : String?
    @objc public var voteAverage : Float = 0.0
    @objc public var voteCount : Int = -1


	enum CodingKeys: String, CodingKey {
		case airDate = "air_date"
		case episodeNumber = "episode_number"
		case id = "id"
		case name = "name"
		case overview = "overview"
		case productionCode = "production_code"
		case seasonNumber = "season_number"
		case showId = "show_id"
		case stillPath = "still_path"
		case voteAverage = "vote_average"
		case voteCount = "vote_count"
	}
    
	required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		airDate = try values.decodeIfPresent(String.self, forKey: .airDate)
		episodeNumber = try values.decodeIfPresent(Int.self, forKey: .episodeNumber) ?? -1
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		name = try values.decodeIfPresent(String.self, forKey: .name)
		overview = try values.decodeIfPresent(String.self, forKey: .overview)
		productionCode = try values.decodeIfPresent(String.self, forKey: .productionCode)
		seasonNumber = try values.decodeIfPresent(Int.self, forKey: .seasonNumber) ?? -1
		showId = try values.decodeIfPresent(Int.self, forKey: .showId) ?? -1
		stillPath = try values.decodeIfPresent(String.self, forKey: .stillPath)
		voteAverage = try values.decodeIfPresent(Float.self, forKey: .voteAverage) ?? -1
		voteCount = try values.decodeIfPresent(Int.self, forKey: .voteCount) ?? 0
	}
}
