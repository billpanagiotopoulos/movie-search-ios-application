//
//	TMDBTrailerResponse.swift
//
//	Create by Vasilis Panagiotopoulos on 9/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

@objc open class TMDBTrailerResponse: NSObject, Codable {

    @objc public var id : Int = -1
    @objc public var results : [TMDBTrailerResult] = []

	enum CodingKeys: String, CodingKey {
		case id = "id"
		case results = "results"
	}
    
    required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id) ?? -1
		results = try values.decodeIfPresent([TMDBTrailerResult].self, forKey: .results) ?? []
	}
}
