//
//	TMDBTrailerResult.swift
//
//	Create by Vasilis Panagiotopoulos on 9/8/2019
//	Copyright © 2019. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

@objc open class TMDBTrailerResult: NSObject, Codable {
	@objc  public let id : String?
	@objc  public let iso31661 : String?
	@objc public let iso6391 : String?
	@objc  public let key : String?
	@objc public let name : String?
	@objc public let site : String?
    @objc public var size : Int = -1
	@objc public let type : String?

	enum CodingKeys: String, CodingKey {
		case id = "id"
		case iso31661 = "iso_3166_1"
		case iso6391 = "iso_639_1"
		case key = "key"
		case name = "name"
		case site = "site"
		case size = "size"
		case type = "type"
	}
    
    required public init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		iso31661 = try values.decodeIfPresent(String.self, forKey: .iso31661)
		iso6391 = try values.decodeIfPresent(String.self, forKey: .iso6391)
		key = try values.decodeIfPresent(String.self, forKey: .key)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		site = try values.decodeIfPresent(String.self, forKey: .site)
		size = try values.decodeIfPresent(Int.self, forKey: .size) ?? -1
		type = try values.decodeIfPresent(String.self, forKey: .type)
	}
}
